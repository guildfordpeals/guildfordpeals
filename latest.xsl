<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:bb="http://bb.ringingworld.co.uk/NS/performances#">
<xsl:template match="/">
<xsl:for-each select="bb:performances/bb:performance/bb:timestamp">
  <xsl:sort select="." order="descending"/>
  <xsl:if test="position() = 1">
  <div><xsl:value-of select="."/></div>
  </xsl:if>
</xsl:for-each>
</xsl:template>
</xsl:stylesheet>

