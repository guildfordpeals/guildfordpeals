<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:bb="http://bb.ringingworld.co.uk/NS/performances#">
<xsl:template match="/">
<xsl:for-each select="bb:performances/bb:performance">
<xsl:sort select="bb:date"/>
  <tr>
    <td><a href="#{@id}"><xsl:value-of select="bb:date"/></a></td>
    <td><xsl:value-of select="bb:title/bb:changes"/>&#160;<xsl:value-of select="bb:title/bb:method"/></td>
    <td><xsl:value-of select="bb:association"/></td>
  </tr>
</xsl:for-each>
</xsl:template>
</xsl:stylesheet>


